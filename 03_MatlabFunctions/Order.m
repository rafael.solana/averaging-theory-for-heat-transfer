function OrderedMatrix = Order(Matrix,Column)

Counter = 1;
OrderedMatrix = Matrix;
Size = size(OrderedMatrix,1);

while Counter ~= 0
    Counter = 0;
    for i=1:Size-1
        if OrderedMatrix(i,Column) > OrderedMatrix(i+1,Column)
            SaveValue = OrderedMatrix(i,:);
            OrderedMatrix(i,:) = OrderedMatrix(i+1,:);
            OrderedMatrix(i+1,:) = SaveValue;
            Counter = Counter + 1;
        end
    end     
end

end

