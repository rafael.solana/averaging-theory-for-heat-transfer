function Matrix = ExcludeColumns(OriginalMatrix,ErasedColumns)
SizeOriginalMatrix = size(OriginalMatrix);
SizeErasedColumns = size(ErasedColumns,1);
ErasedColumnsOrdered = Order(ErasedColumns,1);
Matrix=zeros(SizeOriginalMatrix(1,1),SizeOriginalMatrix(1,2)-SizeErasedColumns);
% for i=1:SizeErasedColumns
%     OriginalMatrix(:,Columns(i,1)) = nan;
% end
% Counter = 1;
% for i=1:SizeOriginalMatrix(1,2)
%     if any(isnan(OriginalMatrix(:,i))==0)
%         Matrix(:,Counter) = OriginalMatrix(:,i);
%         Counter = Counter + 1;
%     end
% end

for i = 1:SizeErasedColumns
    if i==1
        if ErasedColumnsOrdered(1,1) ~=1
            Matrix(:,1:ErasedColumnsOrdered(1,1)-1) = OriginalMatrix(:,1:ErasedColumnsOrdered(1,1)-1);
        end
    else
        Matrix(:,ErasedColumnsOrdered(i-1,1)-(i-1)+1:ErasedColumnsOrdered(i,1)-(i-1)-1) = OriginalMatrix(:,ErasedColumnsOrdered(i-1,1)+1:ErasedColumnsOrdered(i,1)-1);
    end
    if i == SizeErasedColumns
        if ErasedColumnsOrdered(i,1) < SizeOriginalMatrix(1,2)
             Matrix(:,ErasedColumnsOrdered(i,1)-i+1:end) =  OriginalMatrix(:,ErasedColumnsOrdered(i,1)+1:end);
        end
    end
        
end

end