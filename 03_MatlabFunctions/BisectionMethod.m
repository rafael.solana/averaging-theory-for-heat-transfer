function [Root,Error,Counter] = BisectionMethod(Function,StartingPoint,Delta,Curves,Parameters,MaxError, MaxCounter,MaxDisplacements)
F=Function;

if isnan(Curves)
    Size=1;
else
    Size = size(Curves,1);
end


Root=zeros(Size,1);
Error=zeros(Size,1);
Counter=zeros(Size,1);
Deplacements = zeros(Size,1);

for i=1:Size
FirstPoint = StartingPoint;
SecondPoint = FirstPoint + Delta;
ValueAtFirstPoint = F(FirstPoint,Curves(i,:),Parameters);
ValueAtSecondPoint = F(SecondPoint,Curves(i,:),Parameters);
Check = ValueAtFirstPoint*ValueAtSecondPoint;
if Check > 0
    FirstPoint = SecondPoint;
    SecondPoint = SecondPoint + Delta;
    ValueAtFirstPoint = ValueAtSecondPoint;
    ValueAtSecondPoint = F(SecondPoint,Curves(i,:),Parameters);
    Deplacements(i,1) = Deplacements(i,1) + 1;
    if Deplacements(i,1) > MaxDisplacements
        disp('MaxDisplacements reached')
        return
    end
end

while ((abs(ValueAtFirstPoint) > MaxError) && (abs(ValueAtSecondPoint) > MaxError)) && MaxCounter > Counter(i,1)
    MiddlePoint = (1/2)*(FirstPoint + SecondPoint);
    ValueAtMiddlePoint = F(MiddlePoint,Curves(i,:),Parameters);
    if ValueAtMiddlePoint*ValueAtFirstPoint <= 0
        SecondPoint = MiddlePoint;
        ValueAtSecondPoint = F(SecondPoint,Curves(i,:),Parameters);
    else
        FirstPoint=MiddlePoint;
        ValueAtFirstPoint = F(FirstPoint,Curves(i,:),Parameters);
    end
    Counter(i,1) = Counter(i,1) + 1;
end

if Counter(i,1) == MaxCounter
    disp('MaxCounter reached')
    return
end

if (abs(ValueAtFirstPoint)<=abs(ValueAtSecondPoint))
    Root(i,1) = FirstPoint;
    Error(i,1) = abs(ValueAtFirstPoint);
else
     Root(i,1) = SecondPoint;
    Error(i,1) = abs(ValueAtSecondPoint);
end

end

end
