function Vector = IncludeValues(OriginalVector,NewValues)
SizeOriginalVector = size(OriginalVector,1);
SizeNewValues = size(NewValues,1);
Vector = zeros(SizeOriginalVector + SizeNewValues,1);

Vector(1:SizeOriginalVector) = OriginalVector;

NewValuesOrdered = Order(NewValues,2);

for i = 1:SizeNewValues
    Vector(NewValuesOrdered(i,2)+1:end,1) = Vector(NewValuesOrdered(i,2):end-1,1);
    Vector(NewValuesOrdered(i,2),1) = NewValuesOrdered(i,1);
end

end