function y = ExplicitEulerMethod(Function,InitialConditions,Curves,Parameters,ConstantStepSize)
OrderOfSystem = size(InitialConditions,2);
F =Function;

if ConstantStepSize ==1
    dx = Curves(2,1) - Curves(1,1);
else
    dx = Curves(2:end,1) - Curves(1:end-1,1);
end

Size = size(Curves,1);


y=zeros(Size,OrderOfSystem);

y(1,:) = InitialConditions;

for i=2:Size
    if ConstantStepSize ==1
        y(i,:) = y(i-1,:) + dx*F(y(i-1,:),Curves(i-1,:),Parameters);
    else
        y(i,:) = y(i-1,:) + dx(i-1,1)*F(y(i-1,:),Curves(i-1,:),Parameters);
    end
end

end