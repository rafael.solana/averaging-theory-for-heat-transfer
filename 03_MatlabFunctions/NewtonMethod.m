function [Root,Error,Counter] = NewtonMethod(Function, Derivative,Guess, Curves, Parameters, MaxError, MaxCounter)
F= Function;
DF =Derivative;
Size = size(Guess,1);
Root = Guess;
Error=zeros(Size,1);
Counter = zeros(Size,1);

for i=1:Size
    Error(i,1) = F(Root(i,1),Curves(i,:),Parameters);
    while abs(Error(i,1)) > MaxError && Counter(i,1) < MaxCounter
        Gradient = DF(Root(i,1),Curves(i,:),Parameters);
        DeltaRoot = -Error(i,1)/Gradient;
        Root(i,1) = Root(i,1) + DeltaRoot;
        Error(i,1) = F(Root(i,1),Curves(i,:),Parameters);
        Counter(i,1) = Counter(i,1) + 1;
        if Counter == MaxCounter
            disp('NewtonMethod did not converge.')
        end
    end
end
end