function Vector = ExcludeValues(OriginalVector,Positions)
OriginalVector(Positions) = nan;
Vector = OriginalVector(isnan(OriginalVector)==0);
end