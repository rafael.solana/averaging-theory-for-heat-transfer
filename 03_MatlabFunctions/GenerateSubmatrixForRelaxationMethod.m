function Matrix = GenerateSubmatrixForRelaxationMethod(dx,yHalf,Derivative,CurvesHalf,Parameters)
DF = Derivative;
ValuesOfDerivatives=DF(yHalf,CurvesHalf,Parameters);


    Size = size(yHalf,1);
    Matrix =zeros(Size,Size*2);
    for i = 1:Size
        Matrix(:,i) = -(1/2)*dx*ValuesOfDerivatives(:,i);
        Matrix(:,Size + i) = -(1/2)*dx*ValuesOfDerivatives(:,i);
        Matrix(i,i) = -1 + Matrix(i,i);
        Matrix(i,Size + i) = 1 +Matrix(i,Size + i);
    end

end