function Result = RelaxationMethod(Function,Derivative,BoundaryConditions,InitialGuess,Curves,Parameters,MaxChange,MaxCounter)
%Determine the order of the system of ODEs

OrderOfSystem = size(BoundaryConditions,1);

%Get the equations for the derivatives.

F = Function;

%Determine the step size.

dx = Curves(2,1) - Curves(1,1);

%Determine the number of mesh points.

Size = size(Curves,1);

%Initialize J, E, y(including all points) and dy.

J = zeros(OrderOfSystem*(Size-1),OrderOfSystem*Size);
E = zeros(OrderOfSystem*(Size-1),1);
dy = zeros(OrderOfSystem*(Size-1),1);
yAll = zeros(OrderOfSystem*Size,1);

%Initialize y(including all points) with the InitialGuess. This Guess
%should already contain the correct values at the boundary conditions

for i = 1:OrderOfSystem
    yAll(i:OrderOfSystem:end) = InitialGuess(:,i);
end

%Determine the entries of yAll which are prescribed by the boundary
%conditions and eliminate them. The resulting array y does not contain then
%anymore.
%The BoundaryCondition contains the assigned
%value in the first entry, the mesh point at which it holds in the second
%entry and the number of the prescribed variable in the third entry. 

PositionsOfBoundaryConditions = zeros(OrderOfSystem,1);

for i = 1:OrderOfSystem
    PositionsOfBoundaryConditions(i,1) = OrderOfSystem*(BoundaryConditions(i,1)-1) + BoundaryConditions(i,3);
end

BoundaryConditionsToInsert = zeros(OrderOfSystem,2);
BoundaryConditionsToInsert(:,1) = BoundaryConditions(:,2);
BoundaryConditionsToInsert(:,2) = PositionsOfBoundaryConditions;

y = ExcludeValues(yAll,PositionsOfBoundaryConditions);

Counter = 0;

%Perform the iteration to solve the system of ODEs. Maximal values for dy
%and the number of iterations can be defined.

while(Counter ==0 || any(abs(dy) > MaxChange)) && (Counter < MaxCounter)
     
     for i=1:Size-1
         CurvesHalf = (1/2)*(Curves(i,:) + Curves(i+1,:));
         yHalf = (1/2)*(yAll(i*OrderOfSystem+1:(i+1)*OrderOfSystem) + yAll((i-1)*OrderOfSystem+1:i*OrderOfSystem));
         J((i-1)*OrderOfSystem+1:i*OrderOfSystem,(i-1)*OrderOfSystem+1:(i+1)*OrderOfSystem) = GenerateSubmatrixForRelaxationMethod(dx,yHalf,Derivative,CurvesHalf,Parameters);
         E((i-1)*OrderOfSystem+1:i*OrderOfSystem) = yAll(i*OrderOfSystem+1:(i+1)*OrderOfSystem) - yAll((i-1)*OrderOfSystem+1:i*OrderOfSystem) - dx*F(yHalf,CurvesHalf,Parameters);
     end
     
     JReduced = ExcludeColumns(J,PositionsOfBoundaryConditions);
     dy = JReduced\(-E);
     y = y + dy;
     yAll=IncludeValues(y,BoundaryConditionsToInsert);
     Counter = Counter + 1;
end

%Separate the values of the different variables and insert the in the colums of Result. 

 Result=zeros(Size,OrderOfSystem);
 
  for i=1:OrderOfSystem
      Result(:,i) = yAll(i:OrderOfSystem:end);
  end
  
end