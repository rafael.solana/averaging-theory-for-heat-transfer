function Value = InterpolateValue(grid,Data,Position)

%Check if Position is inside of the grid
if (grid(1,1) > Position) || (grid(1,end) < Position)
    disp('Desired position is not included in the grid')
end

%Interpolate the value
Index = find(grid >= Position,1,'first');
if Position == grid(1,Index)
    Value = Data(Index,1);
else
    Value=Data(Index-1,1) + ((Data(Index,1) - Data(Index-1,1))/(grid(1,Index) - grid(1,Index-1)))*(Position - grid(Index-1));
end
end