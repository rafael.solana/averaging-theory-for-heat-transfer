function Integral = TrapezoidRule(grid,Function,InitialValue,ConstantStepSize)

Size = size(grid,2);

if ConstantStepSize == 1
    dx = grid(1,2) - grid(1,1);
else
    dx = grid(1,2:end) - grid(1,1:end-1);
end

Integral = InitialValue;
if ConstantStepSize ==1
    for i = 1:(Size-1)
        Integral = Integral + dx*(1/2)*(Function(i,1)+Function(i+1,1));
    end
else
    for i = 1:(Size-1)
        Integral = Integral + dx(1,i)*(1/2)*(Function(i,1)+Function(i+1,1));
    end
end

end