tic

%% Setup of the test case

%Specify physical properties of the flow
Q=17*(10^(-2))^3; %m^3/s
nu=20*(10^-6); %m^2/s
g=9.81; %m/s^2

%Specify properties of the heat transfer
Pr = 10;
rStartHeating = 0;
NuInit = 1;
HeatTransferParameter = [Pr,rStartHeating];
HeatTransferParameterCWT = [Pr,NuInit];

%Specifiy boundary conditions given in m
r_Start = 0.007500000000000;
h_Start =5.190402860688912e-04;
r_End = 0.030000000000000; 
h_End = 0.003241287939897;

%Specify the settings for the solver
resolution = 2000; %Number of radial points
lambdaThreshold =  -0.8; %Threshold value for the shape parameter used to determine an initial condition for the height and the shape parameter
OrderPoly = [4,4]; % Entry 1: Order of the polynomial used in the region delta_t < h Entry 2: Order of the polynomial in the region delta_t = h
OrderPolyCWT = [4,4]; % Entry 1: Order of the polynomial used in the region delta_t < h Entry 2: Order of the polynomial in the region delta_t = h

%Nondimensionalise the boundary conditions and generate matrix with boundary conditions
ReferenceValues = DetReferenceValues(Q,nu,g);
BoundaryConditions = [ r_Start/ReferenceValues(1,1) h_Start/ReferenceValues(1,2) ; r_End/ReferenceValues(1,1) h_End/ReferenceValues(1,2)];

%% Solution of the averaging theory

%Solve equations for the averaging theory of the flow
[grid,height,lambda] = FlowSolver(BoundaryConditions,resolution,lambdaThreshold);

%Solve temperature field for constant heat flux
[delta_t,Theta0,ThetaSurface,ChangeIndex] = TemperatureFieldSolver(grid,height,lambda,HeatTransferParameter,OrderPoly);

%Solve temperature for constant wall temperature
[delta_tCWT,qh,ThetaSurfaceCWT,ChangeIndexCWT] = TemperatureFieldSolverCWT(grid,height,lambda,HeatTransferParameterCWT,OrderPolyCWT);

%% Postprocessing

%Specify the number of vertical points (evenly spaced in eta)
eta = linspace(0,1,1000)';

%Determine different fields (u,v,f,theta)
uField = ComputeUField(eta,grid,height,lambda);
fField = ComputefField(eta,lambda);
vField = ComputeVField(eta,grid,height,lambda);
if isempty(ChangeIndex)
    CaseSpecifications = [4,1,Pr,nan];
else
    CaseSpecifications = [4,1,Pr,ChangeIndex];
end
ThetaField = ComputeThetaField(eta,height,delta_t,Theta0,ThetaSurface,CaseSpecifications);
if isempty(ChangeIndexCWT)
    CaseSpecificationsCWT = [4,2,Pr,nan];
else
    CaseSpecificationsCWT = [4,2,Pr,ChangeIndexCWT];
end
ThetaFieldCWT = ComputeThetaField(eta,height,delta_tCWT,qh,ThetaSurfaceCWT,CaseSpecificationsCWT);

%Solve temperature field for constant heat flux using a given velocity field.
%In this case the velocity field previously determined with the averaging
%theory is used. This means that the solution coincides with the prevously
%determined one. 
[delta_tNum,Theta0Num,ThetaSurfaceNum,ChangeIndexNum] = TemperatureFieldSolverNum(grid,height,uField,'Analyt',HeatTransferParameter,lambda(1,1));

%Determination of the heat fluxes for all cases treated. Hf means the heat
%flux transported with the flow. Hp means the heat flux transfered from the
%plate to the fluid
[Hf,Hp] = ComputeHeatFlux(grid,eta,height,lambda,delta_t,Theta0,ThetaSurface,CaseSpecifications);
[HfNum,HpNum] = ComputeHeatFlux(grid,eta,height,lambda,delta_tNum,Theta0Num,ThetaSurfaceNum,CaseSpecifications);
[HfCWT,HpCWT] = ComputeHeatFlux(grid,eta,height,lambda,delta_tCWT,qh,ThetaSurfaceCWT,CaseSpecificationsCWT);

precision = 1e-3;
PrTh = FindPrTh(grid,height,lambda,OrderPoly,HeatTransferParameter,precision);

%Plot streamlines

HStart = height(1,1)*[0.02,0.1, 0.2, 0.3, 0.4, 0.5 0.6 0.7 0.8 0.9 1];
RelativeStreamFunctionValue = [0.01,0.05,0.1, 0.2, 0.3, 0.4, 0.5 0.6 0.7 0.8 0.9];
f1 = figure(1);
hold on
DrawStreamlines(grid,height,lambda,HStart,RelativeStreamFunctionValue,[1,1])
hold off

toc





