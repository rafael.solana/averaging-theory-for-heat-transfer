The given package includes the functions for Matlab needed to solve the averaging theory 
developed for describing the flow field [1,2] and temperature field [3] in a circular hydraulic jump.

----------------------------------------------------------------------------------------------------------------------------------------------------------------------

In this folder the script Testcase.m, which solves the averaging theory, is included.
This script can be used as a basis for investigations of the hydraulic jump using the averaging
theory. In it, parameters and boundary conditions for the flow and temperature field are set.
Then the averaging theory for the flow and the temperature field are solved. Finally, the
different scripts for the postprocessing, like the generation of a graph with the streamlines,
are used. Furthermore, the code which solves the averaging theory under assumption of a
constant heat flux condition but with a velocity field given externally is used. As input for
the velocity field, the velocity field determined by the averaging theory is used. Therefore,
the solution is identical to the one determined using the original averaging theory for heat
transfer. 

Please make sure to include all folders ('01_AveragingTheory_Flow', '02_AveragingTheory_TemperatureField', '03_MatlabFunctions' and '04_Postprocessing')
as well as their respective subfolders to your Matlab-Path for proper functioning.


If you use any functionality provided here in your work, please aknowledge it and cite our work [3] as well as the papers on the development of
the averaging theory for the flow [1,2].
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

[1] Bohr, T., Putkaradze, V. & Watanabe, S. (1997) Averaging theory for the structure of hydraulic jumps and separation in laminar free-surface flows. Physical Review Letters 79 (6), 1038–1041.
[2] Watanabe, S., Putkaradze, V., & Bohr, T. (2003). Integral methods for shallow free-surface flows with separation. Journal of fluid mechanics, 480, 233-265.
[3] Solana Gómez, R., Bohr, T., Nielsen, S., Rohlfs, W., Kneer, R. & Askarizadeh, H (2024) Averaging theory for heat transfer in circular hydraulic jumps with a separation bubble. Journal of Fluid Mechanics 979, A41 
