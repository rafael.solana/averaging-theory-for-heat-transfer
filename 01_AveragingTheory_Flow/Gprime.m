%First derivative of the function G with respect to lambda

function Gprime = Gprime(lambda) 
Gprime = -1/15 + 2/105*lambda; 
end