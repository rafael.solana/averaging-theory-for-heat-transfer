% This is the main script for solving the averaging theory for the flow. 
% It generates an evenly spaced radial grid over the radial segement
% specified by the boundary conditions, genaretes an initial guess for the
% flow variables (height and lambda) and solves the averaging theory using a
% relaxation method.
% 
% Input variables: 
% 
% BoundaryCondition: Given as [rInit, hInit; rEnd, hEnd]
% resolution: Number of evenly spaced radial points
% LambdaThreshold: Threshold value for lambda needed for the generation of
% the initial guess

function [grid, height, lambda] = FlowSolver(BoundaryConditions,resolution,LambdaThreshold)

%Generate grid from boundary conditions and prescribed resolution

grid=linspace(BoundaryConditions(1,1),BoundaryConditions(2,1),resolution);

%Generate an initial guess for height and shape parameter.

InitialGuess = GuessFlowVariables(grid,BoundaryConditions,LambdaThreshold);

%Transform boundary conditions into the format necessary for the solver.
%This is further specified in the method of the solver (RelaxationMethod).

BoundaryConditionsSolverFormat = [1 BoundaryConditions(1,2) 1 ; resolution BoundaryConditions(2,2) 1];

%Solve for the height and the shape parameter

FlowVariables = RelaxationMethod(@fFlowVariables,@DfFlowVariables,BoundaryConditionsSolverFormat,InitialGuess,grid',nan,10^(-4),100);
 
%Assign computed values to the height and the shape parameter

height = FlowVariables(:,1);
lambda = FlowVariables(:,2);

end