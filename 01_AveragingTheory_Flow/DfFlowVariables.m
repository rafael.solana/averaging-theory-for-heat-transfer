%Determines the matrix containing the derivatives of h'(derivative of height with respect to the radius) and
%lambda'(derivative of lambda with respect to r) with respect to height and
%lambda. The structure is [dh'/dh, dh'/dlambda; dlambda'/dh, dlambda'/dlambda]

function Df = DfFlowVariables(y,Curves,~)
Df = zeros(2);

r = Curves;
h = y(1,1);
lambda = y(2,1);

Df(1,1) = 3*(5*lambda+3)/(r*h^4);
Df(1,2) = -(5/(r*h^3));
Df(2,1) = (4/(h^2*Gprime(lambda)))*(G(lambda)*((5*lambda + 3)/(r*h^3)) - r*lambda);
Df(2,2) = -(1/Gprime(lambda)^2)*Gprimeprime(lambda)*(4*r*lambda/h + G(lambda)*(h^4-(5*lambda+3))/(r*h^4)) + (1/Gprime(lambda))*((4*r/h) + Gprime(lambda)*((h^4 - (5*lambda + 3))/(r*h^4)) - G(lambda)*(5/(r*h^4)));

end