%Determines a vector containing dheight/dr and dlambda/dr. The structure
%is[dheight/dr; dlambda/dr]

function F = fFlowVariables(y,Curves,~) 
F = zeros(2,1);
r = Curves;
h = y(1,1);
lambda = y(2,1);

F(1,1) = -(5*lambda+3)/(r*h^3); 
F(2,1) = 1/Gprime(lambda) *(4*r*lambda/h + G(lambda)*(h^4-(5*lambda+3))/(r*h^4)); 
end