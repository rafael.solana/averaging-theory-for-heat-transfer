%Generates the initial condition for the application of the relaxation
%method to the averaging theory. This initial condition is constructed
%using the asymptotic solutions before and after the jump. The radius at
%which the initial condition changes from one solution to the other is
%given by the point when the outer solution for lambda reaches the value
%thresholdLambda

function Guess = GuessFlowVariables(grid,BoundaryConditions,LambdaThreshold)
Guess = zeros(length(grid),2);
hGuess = zeros(length(grid), 1); 
lambdaGuess = zeros(length(grid), 1);

lCube = (5*G(-3/5)/4)*BoundaryConditions(1,2)*BoundaryConditions(1,1) - BoundaryConditions(1,1)^3;
hGuessIn = 4/(5*G(-3/5))*(grid.^2 + lCube./grid);
lambdaGuessIn = -3/5 + hGuessIn.^4 /5 - 105/272 * grid.^2.*hGuessIn.^3;

hGuessOut = (BoundaryConditions(2,2)^4 + 12*log(BoundaryConditions(2,1)./grid)).^(1/4);
lambdaGuessOut = 3./(10*grid.^2) .* (3./hGuessOut.^3 - hGuessOut);
 
splitIndex = find(lambdaGuessOut > LambdaThreshold, 1, 'first'); 
 
hGuess(1:splitIndex) = hGuessIn(1:splitIndex); 
lambdaGuess(1:splitIndex) = lambdaGuessIn(1:splitIndex); 
hGuess(splitIndex+1:end) = hGuessOut(splitIndex+1:end);
lambdaGuess(splitIndex+1:end) = lambdaGuessOut(splitIndex+1:end); 

Guess(:,1) = hGuess;
Guess(:,2) = lambdaGuess;
end
