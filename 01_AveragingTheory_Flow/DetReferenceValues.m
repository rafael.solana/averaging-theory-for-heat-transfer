%Determines the reference values used to nondimensionalise the variables
%according to Bohr et al.(1994)

function ReferenceValues = DetReferenceValues(Q,nu,g)

q=Q/(2*pi);
r_ref=((q^5/(g*(nu^3)))^(1/8));
h_ref=(((q^2)*(nu^2)/(g^2))^(1/8));
u_ref =(q*(g^3)*nu)^(1/8);
v_ref = (((g^2)*(nu^6))/(q^2))^(1/8);
ReferenceValues =[r_ref h_ref u_ref v_ref];
end
