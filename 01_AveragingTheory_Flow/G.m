%Function G specified in the averaging theory

function G = G(lambda) 
G = 6/5 - lambda/15 + lambda^2 / 105; 
end