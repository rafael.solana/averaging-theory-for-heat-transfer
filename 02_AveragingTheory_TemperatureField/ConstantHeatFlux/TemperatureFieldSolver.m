% This method determines the parameters needed to describe the temperature
% field according to the averaging theory. First Phi is solved on the entire
% radial domain. Then the first radius for which Phi >= 1 holds is
% determined. From that point on the system of equations describing Theta0
% and ThetaS is used. For both regions the order of the polynomial used to
% describe the temperature has to be chosen (either a third order polynomial
% or a fourth order polynomial). If a fourth order polynomial is used for 
% the region determined by Theta0 and ThetaS and Phi >= 1 holds for the
% initial radius an initial condition for ThetaS is needed. For this values
% different options are available. Right now te option 'ThirdOrderPoly' is
% used if a third order polynomial is used for the region determined by Phi
%Phi >
%If Phi>=1 at the initial radius, an initial value is 


function [delta_t,Theta0,ThetaSurface,ChangeIndex] = TemperatureFieldSolver(grid,height,lambda,HeatTransferParameter,OrderPoly)

%Initialize the variables to be computed

Size = size(grid,2);
delta_t = zeros(Size,1);
Theta0 = zeros(Size,1);
ThetaSurface=zeros(Size,1);

%Get parameters for the heat transfer problem

Pr = HeatTransferParameter(1,1);

%Compute Phi depending on the order chosen for the temperature profile in
%the region where delta_t < h

if OrderPoly(1,1) == 3
    Guess = ones(Size,1);
    Function = @PhiPoly3;
    Derivative = @DPhiPoly3;
    Phi = NewtonMethod(Function,Derivative,Guess,[grid',height,lambda],Pr,10^(-7),100);
elseif OrderPoly(1,1) == 4
    Guess = ones(Size,1);
    Function = @PhiPoly4;
    Derivative = @DPhiPoly4;
    Phi = NewtonMethod(Function,Derivative,Guess,[grid',height,lambda],HeatTransferParameter,10^(-7),100);
end

% Determine where Phi exceeds 1

ChangeIndex = find(Phi' >= 1,1,'first');

%Compute delta_t, Theta0 and ThetaS depending on where Phi exceeds 1 and
%the chosen order for the temperature profile in each region

if isempty(ChangeIndex) == 1
    delta_t = Phi.*height;
    if OrderPoly(1,1) == 3
        Theta0 = (2/3)*delta_t;
    elseif OrderPoly(1,1) == 4
        Theta0 = (1/2)*delta_t;
    end
else
    if ChangeIndex ~= 1
        delta_t(1:ChangeIndex-1,1) = Phi(1:ChangeIndex-1,1).*height(1:ChangeIndex-1,1);
        if OrderPoly(1,1) == 3
            Theta0(1:ChangeIndex-1,1) = (2/3)*delta_t(1:ChangeIndex-1,1);
        elseif OrderPoly(1,1) == 4
            Theta0(1:ChangeIndex-1,1) = (1/2)*delta_t(1:ChangeIndex-1,1);
        end
        delta_t(ChangeIndex:end,1) = height(ChangeIndex:end,1);
        if OrderPoly(1,2) == 3
            Theta0(ChangeIndex:end,1) = DetTheta03(grid(1,ChangeIndex:end),height(ChangeIndex:end,1),lambda(ChangeIndex:end,1),Pr);
            ThetaSurface(ChangeIndex:end,1) = Theta0(ChangeIndex:end,1) - (2/3)*height(ChangeIndex:end,1);
        end
        if OrderPoly(1,2) ==4
            Function = @dThetaSdr;
            ThetaSurface(ChangeIndex:end,1) = ExplicitEulerMethod(Function,0,[grid(1,ChangeIndex:end)',height(ChangeIndex:end),lambda(ChangeIndex:end)],HeatTransferParameter,1);
            Theta0(ChangeIndex:end,1) = DetTheta04(grid(1,ChangeIndex:end)',height(ChangeIndex:end,1),lambda(ChangeIndex:end,1),HeatTransferParameter,ThetaSurface(ChangeIndex:end,1));
        end
    end
    if ChangeIndex == 1
        delta_t = height;
        if OrderPoly(1,2) == 3
            Theta0 = DetTheta03(grid,height,lambda,Pr);
            ThetaSurface = Theta0 - (2/3)*height;
        end
        if OrderPoly(1,2) == 4
            Function = @dThetaSdr;
            %Different methods can be used to determine the initial
            %conditions for ThetaSurface. They are describe in 
            %the function DetInitThetaSurface
            if OrderPoly(1,1) ==3
                MethodInitCond = 'ThirdOrderPoly';
            end
            if OrderPoly(1,1) ==4
                MethodInitCond = 'Analyt';
            end
            % if r0Temperature < r0  
            %     %If r0Temperature(Radius where the thermal boundary layer
            %     %reaches the free surface) < r0, the averaging theory is not
            %     %valid in that region. Therefore, in such a case the
            %     %initial value for ThetaS is computed assuming a third
            %     %order polynomial in the begining. Such polynomial is
            %     %completely defined by the boundary conditions and the energy boundary layer equation.  
            %     InitialValue = GetInitialValueForThetaSurfaceThirdOrderApproach(grid(1,1),height(1,1),lambda(1,1),Pr);
            %     disp('r0Temperature is smaller than r0. Averging theory may not be suited to compute the initial value of the surface temperature.')
            % end
            InitialValue=DetInitThetaSurface(@HeatFluxPhi4,[grid(1,1),height(1,1)],lambda(1,1),HeatTransferParameter,MethodInitCond,[1000,0.1*grid(1,1)]);
            ThetaSurface = ExplicitEulerMethod(Function,InitialValue,[grid',height,lambda],HeatTransferParameter,1);
            Theta0 = DetTheta04(grid',height,lambda,HeatTransferParameter,ThetaSurface);
        end
    end
end
end

