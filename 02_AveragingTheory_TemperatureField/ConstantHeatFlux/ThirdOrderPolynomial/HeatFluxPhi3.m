function result = HeatFluxPhi3(Phi,lambda)
result = ((lambda + 3)/(15))*(Phi^3) - ((5*lambda + 3)/(72))*(Phi^4) + (4*lambda/210)*(Phi^5);
end 