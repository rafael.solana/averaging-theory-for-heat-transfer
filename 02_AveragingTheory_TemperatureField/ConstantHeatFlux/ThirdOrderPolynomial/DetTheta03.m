function Theta0 = DetTheta03(r,h,lambda,Pr)
Flux = (r.^2)'/(2*Pr);
Theta0 = Flux + (-(41/2520)*lambda + (61/120)).*h;
end