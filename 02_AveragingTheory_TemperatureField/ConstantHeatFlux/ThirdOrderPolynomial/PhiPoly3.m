function result = PhiPoly3(Phi,Curves,Pr)
r=Curves(1,1);
h = Curves(1,2);
lambda = Curves(1,3);
result = ((lambda + 3)/(15))*(Phi^3) - ((5*lambda + 3)/(72))*(Phi^4) + (4*lambda/210)*(Phi^5) - (r^2/(2*Pr*h));
end 