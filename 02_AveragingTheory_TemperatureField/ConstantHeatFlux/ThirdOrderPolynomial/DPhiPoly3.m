%Derivative of the polynomial used to determine Phi in case of a constant
%heat flux condition and the usage of a third order polynomial with respect to Phi. 

function result = DPhiPoly3(Phi,Curves,~)
lambda = Curves(1,3);
result = ((lambda +3)/5)*(Phi^2) - ((5*lambda + 3)/18)*(Phi^3) + (2*lambda/21)*(Phi^4);
end