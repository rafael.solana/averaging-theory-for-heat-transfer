function FThetaS = dThetaSdrNum(ThetaS,Field,Curves,Parameters)
u = Field;
r = Curves(1,1);
h = Curves(1,2);
Pr = Parameters;
Theta0 = DetTheta0Num(r,h,u,ThetaS,Pr);

FThetaS = 6/(Pr*(h^2)*u(end,1))*(2*(Theta0-ThetaS) - h);
end