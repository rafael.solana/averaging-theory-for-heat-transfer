function Unconserved = ThermalEnergyConservation(Phi,u,Parameters)
r = Parameters(1,1);
height = Parameters(1,2);
Pr = Parameters(1,3);

Size = size(u,2);
Eta = linspace(0,1,Size)';
Theta = zeros(Size,1);
Index = find(Eta>Phi,1,'first');
if isempty(Index)
    Index = Size;
end
Theta(1:Index,1) = 1/2*Phi - Eta(1:Index,1) + (Eta(1:Index,1).^3)/(Phi^2) - 1/2*(Eta(1:Index,1).^4)/(Phi^3);
Unconserved = r*(height^2)*TrapezoidRule(Eta',u'.*Theta,0,1) - r^2/(2*Pr);
end