function Theta0 = DetTheta0Num(r,height,u,ThetaS,Pr)
Size = size(u,1);
Eta = linspace(0,1,Size)';
Poly1 = u.*(1 - 4*Eta.^3 + 3*Eta.^4);
Poly2 = u.*(height*(Eta - 3*Eta.^3 + 2*Eta.^4) - ThetaS*(4*Eta.^3 - 3*Eta.^4));
%Theta0 = 1/(TrapezRegel(Eta',Poly1,0,1))*(r/(2*Pr*height) + TrapezRegel(Eta',Poly2,0,1));
Xi1 = TrapezoidRule(Eta',Poly1,0,1);
Xi2 = TrapezoidRule(Eta',Poly2,0,1);
Theta0 = 1/(Xi1)*(r/(2*Pr*height) + Xi2);
end