% This script solves the averaging theory for heat transfer numerically for
% a given velocity field.

function [delta_t,Theta0,ThetaS,ChangeIndex] = TemperatureFieldSolverNum(grid,height,uField,MethodInitCond,HeatTransferParameter,lambdaInit)

%Initialize the variables to be computed
Size = size(grid,2);
Phi = zeros(Size,1);
delta_t = zeros(Size,1);
Theta0 = zeros(Size,1);
ThetaS=zeros(Size,1);

%Compute Phi depending on the order chosen for the temperature profile in
%the region where delta_t < h
for i=1:Size
Phi(i,1) = BisectionMethod(@ThermalEnergyConservation,0.01,1,uField(:,i)',[grid(1,i),height(i,1),HeatTransferParameter(1,1)],1e-5,100,1);
end


% Determine where Phi exceeds 1
ChangeIndex = find(Phi' >= 1,1,'first');

%Compute delta_t, Theta0 and ThetaS depending on where Phi exceeds 1 and
%the chosen order for the temperature profile in each region

if isempty(ChangeIndex) == 1
    delta_t = Phi.*height;
    Theta0 = (1/2)*delta_t;
else
    if ChangeIndex ~= 1
        delta_t(1:ChangeIndex-1,1) = Phi(1:ChangeIndex-1,1).*height(1:ChangeIndex-1,1);
        Theta0(1:ChangeIndex-1,1) = (1/2)*delta_t(1:ChangeIndex-1,1);
        delta_t(ChangeIndex:end,1) = height(ChangeIndex:end,1);
        Function = @dThetaSdrNum;
        ThetaS(ChangeIndex:end,1) = ExplicitEulerMethodField(Function,0,uField(:,ChangeIndex:end),[grid(1,ChangeIndex:end)',height(ChangeIndex:end)],HeatTransferParameter(1,1),1);
        for i=ChangeIndex:Size
            Theta0(i,1) = DetTheta0Num(grid(1,i),height(i,1),uField(:,i),ThetaS(i,1),HeatTransferParameter(1,1));
        end
    else
        % If ChangeIndex is equal to 1 the thermal boundary layer
        % reached the film surface before the initial radius. As the used
        % velocity field can have been derived by different means it is not
        % clear what the proper method for determinind an initial condition
        % for ThetaS is. 
        InitialValue = DetInitThetaSurface(@HeatFluxPhi4,[grid(1,1),height(1,1)],lambdaInit,HeatTransferParameter,MethodInitCond,[1000, 0.1*grid(1,1)]);
        Function = @dThetaSdrNum;
        ThetaS(ChangeIndex:end,1) = ExplicitEulerMethodField(Function,InitialValue,uField,[grid',height],HeatTransferParameter(1,1),1);
        for i=ChangeIndex:Size
            Theta0(i,1) = DetTheta0Num(grid(1,i),height(i,1),uField(:,i),ThetaS(i,1),HeatTransferParameter(1,1));
        end
        disp("Warning: The thermal boundary layer reached the film surface before the initial radius. This is not suitable for the numeric solution of the averaging theory.")
    end


end
end

