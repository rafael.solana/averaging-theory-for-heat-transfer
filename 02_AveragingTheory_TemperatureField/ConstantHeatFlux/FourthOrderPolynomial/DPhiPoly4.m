%Derivative of the polynomial used to determine Phi in case of a constant
%heat flux condition and the usage of a fourth order polynomial with respect to Phi. 

function P3 = DPhiPoly4(Phi,Curves,~)
lambda = Curves(1,3);
P3 = ((lambda +3)/10)*(Phi^2) - ((5*lambda + 3)*(1/42))*(Phi^3) + (lambda/28)*(Phi^4);
end