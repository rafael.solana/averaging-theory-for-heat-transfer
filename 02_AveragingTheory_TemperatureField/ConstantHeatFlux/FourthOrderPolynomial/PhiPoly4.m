%Polynomial used to determine Phi for the case of a constant heat flux
%condition and the usage of a fourth order polynomial for the temperature
%profile

function result = PhiPoly4(Phi,Curves,Parameters)
r = Curves(1,1);
h = Curves(1,2);
lambda = Curves(1,3);
Pr = Parameters(1,1);
rStartHeating = Parameters(1,2);
result = ((lambda + 3)/(30))*(Phi^3) - ((5*lambda + 3)*(1/168))*(Phi^4) + (lambda/140)*(Phi^5) - (r^2 - rStartHeating^2)/(2*Pr*h);
end