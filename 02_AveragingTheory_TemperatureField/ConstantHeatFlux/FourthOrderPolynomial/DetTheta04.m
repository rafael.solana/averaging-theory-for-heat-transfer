% Determines Theta0 for the case of a constant heat flux using a fourth
% order polynomial. Besides r,h,Pr and ThetaSurface, the radius at for
% which the heat exchange between the fluid and the wall starts occuring. 

function Theta0 =DetTheta04(r,h,lambda,HeatingParameters,ThetaSurface)
Pr = HeatingParameters(1,1);
rStartHeating = HeatingParameters(1,2);
Flux = (r.^2 - rStartHeating^2)/(2*Pr);
M = (1/30)*lambda - (19/35);
N = (1/168)*lambda + (41/280);
Theta0 = (1./(1+M)).*(Flux + N.*h + M.*ThetaSurface);
end