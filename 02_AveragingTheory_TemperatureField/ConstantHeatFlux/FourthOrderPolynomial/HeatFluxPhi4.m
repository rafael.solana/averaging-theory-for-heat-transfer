%Polynomial used to determine Phi without the term independent of Phi 
%for the case of a constant heat flux condition and the usage 
%of a fourth order polynomial for the temperature profile 

function result = HeatFluxPhi4(Phi,lambda)
result = ((lambda + 3)/(30)).*(Phi.^3) - ((5*lambda + 3)*(1/168)).*(Phi.^4) + (lambda/140).*(Phi.^5);
end