%Determination of the dThetaSdr for the case a constant heat flux condition and the usage of a fourht order polynomial 

function FSurfTemp = dThetaSdr(ThetaSurface,Curves,Parameters)
r = Curves(1,1);
h = Curves(1,2);
lambda = Curves(1,3);
Pr = Parameters(1,1);
 FSurfTemp = r*(12*DetTheta04(r,h,lambda,Parameters,ThetaSurface) - 6 *h - 12*ThetaSurface)/(Pr*h*(-(1/6)*lambda+(3/2)));
end