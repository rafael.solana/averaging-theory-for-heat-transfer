%Determines an initial value for ThetaSurface for the case of a constant heat flux
%condition if the thermal boundary layer reached the surface before the
%initial radius. The working principle of all the methods is discribed in
%more detail above the different options. 

function InitialValue = DetInitThetaSurface(Polynom,BoundaryConditions,lambda,HeatTransferParameter,Method,Param)

%Option 1: Determination of ThetaSurface at the beginning using a third
%order polynomial
if strcmp(Method,'ThirdOrderPoly')
    Theta0 = DetTheta03(BoundaryConditions(1,1),BoundaryConditions(1,2),lambda,HeatTransferParameter(1,1));
    InitialValue = Theta0 - (2/3)*BoundaryConditions(1,2);
end

%Option 2: Determine the point where Phi = 1. This is done using an
%analytic relation which can be determined using the asyptotic approach for
%the height and assuming lambda = -3/5.
%Then determine the height and lambda betweenthis point and the initial radius. 
%This is done using the asympotic analysis of the averaging theory. 
%Finally, integrate ThetaSurface in this region starting with ThetaSurface = 0.
if strcmp(Method,'Analyt')
    Resolution = Param(1,1);
    Pr = HeatTransferParameter(1,1);
    F = Polynom;
    K = 2*Pr*F(1,-3/5)*(4/(5*G(-3/5)));
    lCube = (5*G(-3/5)/4)*BoundaryConditions(1,2)*BoundaryConditions(1,1) - BoundaryConditions(1,1)^3;
    r_Start = (K/(1- K))^(1/3)*lCube;
    r_End = BoundaryConditions(1,1);
    grid = linspace(r_Start,r_End,Resolution);
    h = 4/(5*G(-3/5))*(grid.^2 + lCube./grid);
    lambda = -3/5 + h.^4 /5 - 105/272 * grid.^2.*h.^3;
    ThetaSurface = ExplicitEulerMethod(@dThetaSdr,0,[grid',h',lambda'],HeatTransferParameter,1);
    InitialValue = ThetaSurface(end,1);
end

%Option 3: Determine height and lambda before the jump using the asymptotic
%analysis of the averaging theory.The smallest radius included has to be given manually. 
%Determine the point in this region where Phi = 1 numerically. 
%If Phi = 1 does not hold at any point an error is returned.
%If an appropriate point is found ThetaSurface is integrated from this point 
%until rStart beginning with ThetaSurface = 0.
if strcmp(Method,'AnalytNum')
    Resolution = Param(1,1);
    rMin = Param(1,2);
    lCube = (5*G(-3/5)/4)*BoundaryConditions(1,2)*BoundaryConditions(1,1) - BoundaryConditions(1,1)^3;
    r_End = BoundaryConditions(1,1);
    grid = linspace(rMin,r_End,Resolution);
    h = 4/(5*G(-3/5))*(grid.^2 + lCube./grid);
    lambda = -3/5 + h.^4 /5 - 105/272 * grid.^2.*h.^3;
    Guess = ones(Resolution,1);
    Function = @PhiPoly4;
    Derivative = @DPhiPoly4;
    Phi = NewtonMethod(Function,Derivative,Guess,[grid',h',lambda'],HeatTransferParameter,10^(-7),100);
    ChangeIndex = find(Phi' >= 1,1,'first');
    if isempty(ChangeIndex)
        disp("rT0 could not be found")
    end
    ThetaSurface = ExplicitEulerMethod(@dThetaSdr,0,[grid(1,ChangeIndex:end)',h(1,ChangeIndex:end)',lambda(1,ChangeIndex:end)'],HeatTransferParameter,1);
    InitialValue = ThetaSurface(end,1);
end
end
