function dqh = dqhdr3(qh,Curves,Parameters)

r = Curves(1,1);
height = Curves(1,2);
lambda = Curves(1,3);
Pr = Parameters(1,1);

Zeta1 = 41/2520*lambda - 61/120;
dlambdadr = 1/Gprime(lambda)*(4*r*lambda/height + G(lambda)*(height^4-(5*lambda+3))/(r*height^4)); 
Zeta2 = r/(Pr*height) - 41/2520*dlambdadr;

dqh = Zeta2/Zeta1*qh;

end