function dPhi = dPhidr3CWT(Phi,Curves,Parameter)
r = Curves(1,1);
height= Curves(1,2);
lambda = Curves(1,3);
Pr = Parameter(1,1);

Zeta1 = (lambda + 3)/5*Phi - (5*lambda+3)/16*Phi^2 + lambda/140*Phi^3;
dlambdadr = 1/Gprime(lambda) *(4*r*lambda/height + G(lambda)*(height^4-(5*lambda+3))/(r*height^4)); 
Zeta2 = ((1/10)*Phi^2 - (5/48)*Phi^3 + (1/35)*Phi^4)*dlambdadr;

dPhi = 1/Zeta1*((3*r)/(2*Phi*height*Pr) - Zeta2);
end
