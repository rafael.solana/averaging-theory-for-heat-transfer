function [delta_t,qh,ThetaS,ChangeIndex] = TemperatureFieldSolverCWT(grid,height,lambda,HeatTransferParameter,OrderPoly)

%Initialize the variables to be computed

Size = size(grid,2);
delta_t = zeros(Size,1);
qh = zeros(Size,1);
ThetaS=zeros(Size,1);

%Get parameters for the heat transfer problem

Pr = HeatTransferParameter(1,1);
NuInit = HeatTransferParameter(1,2);

%Determine which equation is needed at the beginning of the flow and the
%value of the initial condition. For this the Nusselt number is determined
%using the initial height of the flow as reference distance.

if OrderPoly(1,1) ==3
    PhiInit = 3/(2*NuInit);
elseif OrderPoly(1,1) == 4
    PhiInit = 2/NuInit;
end

if PhiInit < 1
    if OrderPoly(1,1) == 3
        FunctionPhi = @dPhidr3CWT;
        Phi = ExplicitEulerMethod(FunctionPhi,PhiInit,[grid',height,lambda],Pr,1);
    elseif OrderPoly(1,1) == 4
        FunctionPhi = @dPhidr4CWT;
        Phi = ExplicitEulerMethod(FunctionPhi,PhiInit,[grid',height,lambda],Pr,1);
    end
    ChangeIndex = find(Phi' >= 1,1,'first');
    if isempty(ChangeIndex) ~= 1
        delta_t(1:ChangeIndex,1) = Phi(1:ChangeIndex,1).*height(1:ChangeIndex,1);
        delta_t(ChangeIndex:end,1) = height(ChangeIndex:end,1);
    else
        delta_t = Phi.*height;
        if OrderPoly(1,2) == 3
            qh = 3./(2*Phi);
        elseif OrderPoly(1,2) == 4
            qh = 2./Phi;
        end
    end
else
    ChangeIndex = 1;
    delta_t = height;
end

if isempty(ChangeIndex) ~= 1
    if ChangeIndex == 1
        qhInit = NuInit;
    else
        if OrderPoly(1,1) == 3
            qhInit = 3/2;
            qh(1:ChangeIndex-1) =3./(2*Phi(1:ChangeIndex-1,1));
        elseif OrderPoly(1,1) == 4
            qhInit = 2;
            qh(1:ChangeIndex-1) = 2./Phi(1:ChangeIndex-1,1);
        end
    end
    if OrderPoly(1,2) == 3
        Function = @dqhdr3;
        qh(ChangeIndex:end,1)= ExplicitEulerMethod(Function,qhInit,[grid(1,ChangeIndex:end)',height(ChangeIndex:end,1),lambda(ChangeIndex:end,1)],Pr,1);
    elseif OrderPoly(1,2) == 4
        Function = @dqhdr4;
        ThetaSInit = 0;
        Solution = ExplicitEulerMethod(Function,[qhInit,ThetaSInit],[grid(1,ChangeIndex:end)',height(ChangeIndex:end,1),lambda(ChangeIndex:end,1)],Pr,1);
        qh(ChangeIndex:end,1) = Solution(:,1);
        ThetaS(ChangeIndex:end,1) = Solution(:,2);
        %If ChangeIndex is equal to 1 an additional condition for ThetaS is
        %needed at the film surface. As the equation for Phi as well as the equation for Theta0 using a third order polynomial
        %is not an algebraic equation as in the case for a constant heat flux it is
        %not as easy to derive a method to determine this initial
        %condition. Right now the initial condition can be set by hand in
        %this script. 
    end
end

end