function dPhi = dPhidr4(Phi,Curves,Parameter)
r = Curves(1,1);
height= Curves(1,2);
lambda = Curves(1,3);
Pr = Parameter(1,1);

Zeta1 = (2*lambda + 6)/15*Phi - (5*lambda+3)/28*Phi^2 + 4*lambda/70*Phi^3;
dlambdadr = 1/Gprime(lambda) *(4*r*lambda/height + G(lambda)*(height^4-(5*lambda+3))/(r*height^4)); 
Zeta2 = ((1/15)*Phi^2 - (5/84)*Phi^3 + (1/70)*Phi^4)*dlambdadr;

dPhi = 1/Zeta1*((2*r)/(Phi*height*Pr) - Zeta2);
end