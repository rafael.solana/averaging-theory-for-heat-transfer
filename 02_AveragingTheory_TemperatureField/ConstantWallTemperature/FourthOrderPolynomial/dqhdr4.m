function dq = dqhdr4(FuncValues,Curves,Parameters)

qh = FuncValues(1,1);
ThetaS = FuncValues(1,2);

r = Curves(1,1);
height = Curves(1,2);
lambda = Curves(1,3);
Pr = Parameters(1,1);

fEtaEqualOne = -(1/6)*lambda+(3/2);
dlambdadr = 1/Gprime(lambda)*(4*r*lambda/height + G(lambda)*(height^4-(5*lambda+3))/(r*height^4)); 
N = (1/168)*lambda + 41/280;
M = (1/30)*lambda - 19/35;
dN = (1/168)*dlambdadr;
dM = (1/30)*dlambdadr;

Zeta1 = (6*r*M)/(Pr*height*fEtaEqualOne) - r/(Pr*height) - dN;
Zeta2 = dM - (12*r*M)/(Pr*height*fEtaEqualOne);

dqh = 1/N*(Zeta1*qh + Zeta2*(1-ThetaS));
dThetaS = (6*r)/(Pr*height*fEtaEqualOne)*(2-qh-2*ThetaS);

dq = [dqh,dThetaS];
end