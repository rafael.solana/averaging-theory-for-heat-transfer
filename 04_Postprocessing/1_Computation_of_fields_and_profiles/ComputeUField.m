function Vel = ComputeUField(eta,grid,height,lambda)

SizeR = size(grid,2);
SizeEta = size(eta,1);
Vel=zeros(SizeEta,SizeR);

for i=1:SizeR
Vel(:,i) = (1/(grid(1,i)*height(i,1)))*((lambda(i,1) + 3)*eta - ((5*lambda(i,1) + 3)/2)*eta.^2 + (4*lambda(i,1)/3)*eta.^3);
end
end
