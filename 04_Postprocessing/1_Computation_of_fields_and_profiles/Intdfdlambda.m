function Intdf = Intdfdlambda(eta)
Intdf = (1/2)*eta.^2 - (5/6)*eta.^3 + (1/3)*eta.^4;
end