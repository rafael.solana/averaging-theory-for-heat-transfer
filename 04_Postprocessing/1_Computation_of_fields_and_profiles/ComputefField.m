function f = ComputefField(eta,lambda)

SizeR = size(lambda,1);
SizeEta = size(eta,1);
f=zeros(SizeEta,SizeR);

for i=1:SizeR
f(:,i) = ((lambda(i,1) + 3)*eta - ((5*lambda(i,1) + 3)/2)*eta.^2 + (4*lambda(i,1)/3)*eta.^3);
end
end