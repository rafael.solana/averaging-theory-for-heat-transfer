function TProfile = ComputeThetaField(eta,height,delta_t,ParameterOnPlate,ThetaSurface,CaseSpecifications)

%Computes the temperature profile in nondimensional form
%BoundaryCondition = 1: Constant heat flux
%BoundaryCondition = 2: Constant plate temperature

OrderOfPolynomial = CaseSpecifications(1,1);
BoundaryCondition = CaseSpecifications(1,2);
ChangeIndex = CaseSpecifications(1,4);

if BoundaryCondition == 1
    Theta0 = ParameterOnPlate;
elseif BoundaryCondition == 2
    qh = ParameterOnPlate;
end

SizeR = size(delta_t,1);
SizeEta = size(eta,1);
TProfile = zeros(SizeEta,SizeR);

if BoundaryCondition == 1
    for i=1:SizeR
        if ThetaSurface(i,1) == 0
            for j = 1:SizeEta
                if eta(j,1)*height(i,1) < delta_t(i,1)
                    if OrderOfPolynomial == 4
                        TProfile(j,i) = delta_t(i,1)*((1/2) - eta(j,1)*(height(i,1)/delta_t(i,1)) + (eta(j,1)*(height(i,1)/delta_t(i,1))).^3 -  (1/2)*(eta(j,1)*(height(i,1)/delta_t(i,1))).^4);
                    elseif OrderOfPolynomial == 3
                        TProfile(j,i) = delta_t(i,1)*((2/3) - eta(j,1)*(height(i,1)/delta_t(i,1)) + (1/3)*(eta(j,1)*(height(i,1)/delta_t(i,1))).^3);
                    end
                else
                    TProfile(j,i) = 0;
                end
            end
        else
            TProfile(:,i) = Theta0(i,1)*(1 - 4*eta.^3 + 3*eta.^4)...
                - delta_t(i,1)*(eta - 3*(eta.^3) + 2*eta.^4)...
                + ThetaSurface(i,1)*(4*eta.^3 - 3*eta.^4);
        end
    end
elseif BoundaryCondition == 2
    for i=1:SizeR
        if ThetaSurface(i,1) == 0 && ChangeIndex ~= 1
            for j = 1:SizeEta
                if eta(j,1)*height(i,1) < delta_t(i,1)
                    if OrderOfPolynomial == 4
                        TProfile(j,i) = 1 - 2*eta(j,1)*(height(i,1)/delta_t(i,1)) + 2*(eta(j,1)*(height(i,1)/delta_t(i,1))).^3 - (eta(j,1)*(height(i,1)/delta_t(i,1))).^4;
                    elseif OrderOfPolynomial == 3
                        TProfile(j,i) = 1 - (3/2)*eta(j,1)*(height(i,1)/delta_t(i,1)) + (1/2)*(eta(j,1)*(height(i,1)/delta_t(i,1))).^3;
                    end
                else
                    TProfile(j,i) = 0;
                end
            end
        else
            TProfile(:,i) = (1 - 4*eta.^3 + 3*eta.^4)...
                - qh(i,1).*(eta - 3*(eta.^3) + 2*eta.^4)...
                + ThetaSurface(i,1)*(4*eta.^3 - 3*eta.^4);
        end
    end
end

end