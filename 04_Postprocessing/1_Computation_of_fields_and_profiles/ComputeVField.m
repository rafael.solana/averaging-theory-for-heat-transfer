function V = ComputeVField(eta,grid,height,lambda)

SizeR = size(grid,2);
SizeEta = size(eta,1);
V=zeros(SizeEta,SizeR);

Intdf = Intdfdlambda(eta);
for i=1:SizeR
U = ((1/(grid(1,i)*height(i,1)))*((lambda(i,1) + 3)*eta - ((5*lambda(i,1) + 3)/2)*eta.^2 + (4*lambda(i,1)/3)*eta.^3));
F = fFlowVariables([height(i,1);lambda(i,1)],grid(1,i),nan);
dhdr = F(1,1);
dlambdadr = F(2,1);
V(:,i) = dhdr*U.*eta - (1/grid(1,i))*dlambdadr*Intdf;
end
end
