function IndicesRecirculation = GetIndicesRecirculation(lambda)

IndicesRecirculation = zeros(1,2);
Index1=find(lambda' < -3,1,'first');
if (abs(lambda(Index1,1) + 3) <= abs(lambda(Index1-1,1) + 3))
    IndicesRecirculation(1,1) = Index1;
else
    IndicesRecirculation(1,1) = Index1 - 1;
end



Index2=find(lambda' < -3,1,'last');
if (abs(lambda(Index2,1) + 3) <= abs(lambda(Index2+1,1) + 3))
    IndicesRecirculation(1,2) = Index2;
else
    IndicesRecirculation(1,2) = Index2 + 1;
end



end