% Determines the indices between which the recirculation bubble is located.
% Furthermore, it gives height and eta values where the function characterising the velocity profile, its integral or its derivative are zero.  

function [FZero,DFZero,IntFZero,EtaFZero,EtaDFZero,EtaIntFZero,gridRecirculation,lambdaRecirculation,heightRecirculation] = AnalyseRecirculation(grid,lambda,height)
IndicesRecirculation = GetIndicesRecirculation(lambda);
lambdaRecirculation = lambda(IndicesRecirculation(1,1):IndicesRecirculation(1,2),1);
gridRecirculation = grid(1,IndicesRecirculation(1,1):IndicesRecirculation(1,2));
heightRecirculation=height(IndicesRecirculation(1,1):IndicesRecirculation(1,2),1);
EtaDFZero = (2*lambdaRecirculation + 6)./(8*lambdaRecirculation);
EtaFZero = (15*lambdaRecirculation + 9)./(16*lambdaRecirculation) - sqrt((33*lambdaRecirculation.^2 - 306*lambdaRecirculation + 81)./(256*lambdaRecirculation.^2));
EtaIntFZero = (5*lambdaRecirculation + 3)./(4*lambdaRecirculation) - sqrt((lambdaRecirculation.^2 - 42*lambdaRecirculation + 9)./(16*lambdaRecirculation.^2));
FZero = EtaFZero.*heightRecirculation;
DFZero = EtaDFZero.*heightRecirculation;
IntFZero =EtaIntFZero.*heightRecirculation;
end