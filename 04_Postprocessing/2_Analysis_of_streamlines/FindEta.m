function Eta = FindEta(lambda,Integral,InitialGuess)
Const = Integral;
Eta = InitialGuess;
err=IntegrateF(lambda,Eta)-Const;
errMax = 10^(-6);
counterMax=1000;
counter=0;
while abs(err)>errMax && counter<counterMax
    dEta = -err/fVel(Eta,lambda);
    Eta = Eta+dEta;
    err=IntegrateF(lambda,Eta)-Const;
    counter = counter+1;
end
end