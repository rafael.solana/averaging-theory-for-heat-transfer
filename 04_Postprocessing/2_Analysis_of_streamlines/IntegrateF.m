function FInt = IntegrateF(lambda,eta)
FInt=((lambda+3)/2)*eta^2 - ((5*lambda+3)/6)*eta^3 + (lambda/3)*eta^4;
end