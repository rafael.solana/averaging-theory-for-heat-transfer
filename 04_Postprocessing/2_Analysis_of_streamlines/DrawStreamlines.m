function DrawStreamlines(grid,height,lambda,HStart,RelativeStreamFunctionValue,RescalingFactors)
[~,~,~,EtaFZero,~,~,~,lambdaRecirculation] = AnalyseRecirculation(grid,lambda,height);
IndicesRecirculation = GetIndicesRecirculation(lambda);
[~,IndexVortexCenter] = min(lambdaRecirculation);
MinStreamFunction=IntegrateF(lambdaRecirculation(IndexVortexCenter,1),EtaFZero(IndexVortexCenter,1));
StreamFunctionValue = MinStreamFunction*RelativeStreamFunctionValue;

for i = 1:size(HStart,2)
    OpenStreamline = GetOpenStreamline(height,lambda,HStart(1,i));
    plot(grid*RescalingFactors(1,1),OpenStreamline*RescalingFactors(1,2),'color','black','LineWidth',1.3)
end

for i = 1:size(StreamFunctionValue,2)
    [LowerLine,UpperLine,IndexBeginning,IndexEnd] = GetClosedStreamline(height,lambda,EtaFZero,IndicesRecirculation,StreamFunctionValue(1,i));
    Size = IndexEnd-IndexBeginning+1;
    gridCirc = zeros(1,Size*2);
    EntireLine = zeros(1,Size*2);
    gridCirc(1,1:Size) = grid(1,IndexBeginning:IndexEnd);
    EntireLine(1,1:Size) = LowerLine;
    for j = 1:Size
        gridCirc(1,Size+j) = grid(1,IndexEnd+1-j);
        EntireLine(1,Size+j) = UpperLine(end+1-j,1);
    end

plot(gridCirc*RescalingFactors(1,1),EntireLine*RescalingFactors(1,2),'color',[0,0,0],'LineWidth',1.3);
    
end

end