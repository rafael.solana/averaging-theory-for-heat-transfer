function Eta = FindEta2(lambda,Integral,StartingPoint,Delta)
Eta = BisectionMethod(@IntegrateF2,StartingPoint,Delta,lambda,Integral,10^(-10),100,1);
end
