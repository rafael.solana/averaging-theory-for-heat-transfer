function OpenStreamline = GetOpenStreamline(height,lambda,HStart)
Size = size(height,1);
OpenStreamline=zeros(Size,1);
OpenStreamline(1,1)=HStart;
EtaStart=HStart/height(1,1);
Const = IntegrateF(lambda(1,1),EtaStart);
for i=2:Size
    Eta = 1;
    err=IntegrateF(lambda(i,1),Eta)-Const;
    errMax = 10^(-6);
    counterMax=1000;
    counter=0;
    while abs(err)>errMax && counter<counterMax
        dEta = -err/ComputefField(Eta,lambda(i,1));
        Eta = Eta+dEta;
        err=IntegrateF(lambda(i,1),Eta)-Const;
        counter = counter+1;
    end
    OpenStreamline(i,1)=Eta*height(i,1);
end
end