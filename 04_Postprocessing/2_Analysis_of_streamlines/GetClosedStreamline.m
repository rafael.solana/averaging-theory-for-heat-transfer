function [LowerLine,UpperLine,IndexBeginning,IndexEnd] = GetClosedStreamline(height,lambda,EtaFZero,RecirculationIndices,StreamFunctionValue)

%Determine the values of the stream function on the line containing all
%points which satisfy the condition u = 0
StreamFunctionOnF = zeros(RecirculationIndices(1,2) - RecirculationIndices(1,1) +1 ,1);
for i = 1:RecirculationIndices(1,2) - RecirculationIndices(1,1) +1
StreamFunctionOnF(i,1) = IntegrateF(lambda(i+RecirculationIndices(1,1)-1,1),EtaFZero(i,1));
end

%Determine the indices of the radii between which the closed streamline
%defined by the given stream function value (StreamFunctionValue) is
%enclosed
ProductStreamFunctionOnF = (StreamFunctionOnF(1:end-1,1)-StreamFunctionValue).*(StreamFunctionOnF(2:end,1)-StreamFunctionValue);  
IndexBeginning = RecirculationIndices(1,1) + find(ProductStreamFunctionOnF<0,1,'first');
IndexEnd = RecirculationIndices(1,1) - 1 + find(ProductStreamFunctionOnF<0,1,'last');
EnclosedRegionSize= IndexEnd - IndexBeginning +1;
UpperLine = zeros(EnclosedRegionSize,1);
LowerLine = zeros(EnclosedRegionSize,1);

%Determines the low and high point of the closed streamline at each radius
%between the enclosing radii. The results are saved in the two vector
%LowerLine and UpperLine. At the delimiting radii the low and high point
%coincide on the line which satisfies the condition u = 0.
for i=1:EnclosedRegionSize
    if i == 1
        EtaBoth = EtaFZero(IndexBeginning-RecirculationIndices(1,1) + 1);
        LowerLine(i,1) = height(IndexBeginning,1)*EtaBoth;
        UpperLine(i,1) = height(IndexBeginning,1)*EtaBoth;
        
    elseif i == EnclosedRegionSize
        EtaBoth = EtaFZero(IndexEnd-RecirculationIndices(1,1) + 1);
        LowerLine(i,1) = height(IndexEnd,1)*EtaBoth;
        UpperLine(i,1) = height(IndexEnd,1)*EtaBoth;
    else
        EtaF = EtaFZero(IndexBeginning-RecirculationIndices(1,1) +i);
        %delta = 0.1;
        % UpperLineEta = FindEta2(lambda(IndexBeginning-1+i,1),StreamFunctionValue,EtaF+delta,EtaF,1-EtaF);
        % LowerLineEta = FindEta2(lambda(IndexBeginning-1+i,1),StreamFunctionValue,EtaF-delta,0,EtaF);
        UpperLineEta = FindEta2(lambda(IndexBeginning-1+i,1),StreamFunctionValue,EtaF,1-EtaF);
        LowerLineEta = FindEta2(lambda(IndexBeginning-1+i,1),StreamFunctionValue,0,EtaF);
        UpperLine(i,1) = UpperLineEta*height(IndexBeginning-1+i,1);
        LowerLine(i,1) = LowerLineEta*height(IndexBeginning-1+i,1);
    end 
end
end