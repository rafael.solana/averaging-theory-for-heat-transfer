function FInt = IntegrateF2(eta,lambda,IntKonst)
FInt=((lambda+3)/2)*eta^2 - ((5*lambda+3)/6)*eta^3 + (lambda/3)*eta^4 - IntKonst;
end