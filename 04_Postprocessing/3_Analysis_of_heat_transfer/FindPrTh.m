function Pr = FindPrTh(grid,height,lambda,OrderPoly,HeatTransferParameter,precision)
Pr = HeatTransferParameter(1,1);
rStartHeating = HeatTransferParameter(1,2);
PrLowerLimit = 0;
PrUpperLimit = 0;
counter = 0;
MaxCounter = 1000;

[~,~,~,ChangeIndex] = TemperatureFieldSolver(grid,height,lambda,[Pr,rStartHeating],OrderPoly);
if isempty(ChangeIndex)
    PrUpperLimit = Pr;
else
    PrLowerLimit = Pr;
end

while (PrLowerLimit == 0 || PrUpperLimit == 0) && counter < MaxCounter
    if PrLowerLimit ~= 0
        Pr = Pr + 1;
        [~,~,~,ChangeIndex] = TemperatureFieldSolver(grid,height,lambda,[Pr,rStartHeating],OrderPoly);
        if isempty(ChangeIndex)
            PrUpperLimit = Pr;
        else
            PrLowerLimit = Pr;
        end
    else
        Pr = Pr - 1;
        [~,~,~,ChangeIndex] = TemperatureFieldSolver(grid,height,lambda,[Pr,rStartHeating],OrderPoly);
        if isempty(ChangeIndex) == false
            PrLowerLimit = Pr;
        else
            PrUpperLimit = Pr;
        end
    end   
    counter = counter + 1;
end

if counter == MaxCounter
    disp('counter reached maximum value') 
end

counter = 0;

if (PrLowerLimit ~= 0 && PrUpperLimit ~= 0)
    while (PrUpperLimit - PrLowerLimit) > precision && counter < MaxCounter
        Pr = 0.5*(PrLowerLimit + PrUpperLimit);
        [~,~,~,ChangeIndex] = TemperatureFieldSolver(grid,height,lambda,[Pr,rStartHeating],OrderPoly);
        if isempty(ChangeIndex)
            PrUpperLimit = Pr;
        else
            PrLowerLimit = Pr;
        end
        counter = counter + 1;
    end
end

end