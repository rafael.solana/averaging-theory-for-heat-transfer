function [Hf,Hp] = ComputeHeatFlux(grid,eta,height,lambda,delta_t,ParameterOnPlate,ThetaSurface,CaseSpecifications)

%Hf is the heat flux transported with the flow. Hp is the heat flux
%tranfered from the plate to the flow.

BoundaryCondition = CaseSpecifications(1,2);
Pr = CaseSpecifications(1,3);

Size = size(grid,2);
Hf = zeros(Size,1);
Hp = zeros(Size,1);

ThetaField = ComputeThetaField(eta,height,delta_t,ParameterOnPlate,ThetaSurface,CaseSpecifications);
fField = ComputefField(eta,lambda);

FluxField = ThetaField.*fField;

for i =1:Size
    Hf(i,1) = TrapezoidRule(eta',FluxField(:,i),0,1);
end

if BoundaryCondition == 1
    Hp = (grid.^2)'/(2*Pr);
elseif BoundaryCondition == 2
    rq = grid'.*ParameterOnPlate./height;
    Hp(1,1) = Hf(1,1);
    for i = 2:Size
        Hp(i,1) = TrapezoidRule(grid(1,1:i),rq(1:i,1),Hf(1,1)*Pr,1);
    end
    Hp = Hp/Pr;
    Hp(1,1) = Hf(1,1);
end

end

